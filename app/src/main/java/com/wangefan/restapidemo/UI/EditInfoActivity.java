package com.wangefan.restapidemo.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.wangefan.restapidemo.R;
import com.wangefan.restapidemo.Utility.ApiClient;
import com.wangefan.restapidemo.Utility.RESTAPIDemoSettings;
import com.wangefan.restapidemo.Utility.SystemUtility;

public class EditInfoActivity extends AppCompatActivity {
    /**
     * Message ID to category messages.
     */
    private static final int MSG_UPDATEZONE_FAIL = 0;
    private static final int MSG_UPDATEZONE_SUCCESS = 1;

    private TextView mTvHello;
    private EditText mEdTimezone;
    private FloatingActionButton mFab;
    private ProgressDialog mPrgsDialog;
    private Handler mUIHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATEZONE_FAIL:
                    showProgress(false);
                    SystemUtility.showToast(EditInfoActivity.this, R.string.error_timezone);
                    break;
                case MSG_UPDATEZONE_SUCCESS:
                    SystemUtility.showToast(EditInfoActivity.this, R.string.update_timezone_ok);
                    mTvHello.setText(String.format(getString(R.string.hello_string), RESTAPIDemoSettings.getUserName(), RESTAPIDemoSettings.getTimezone()));
                    showProgress(false);
                    break;
                default:
                    break;
            }
        }
    };

    private void showProgress(boolean bShow) {
        if(bShow) {
            mPrgsDialog = ProgressDialog.show(EditInfoActivity.this, getString(R.string.updating_progress_title), getString(R.string.updating_progress));
        } else {
            if(mPrgsDialog != null) {
                mPrgsDialog.dismiss();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //UI controls
        mTvHello = (TextView) findViewById(R.id.tv_hello);
        mTvHello.setText(String.format(getString(R.string.hello_string), RESTAPIDemoSettings.getUserName(), RESTAPIDemoSettings.getTimezone()));

        mEdTimezone = (EditText) findViewById(R.id.ed_timezone);
        mEdTimezone.setText(String.valueOf(RESTAPIDemoSettings.getTimezone()));

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int nTimezone = Integer.valueOf(String.valueOf(mEdTimezone.getText()));
                final int MAX_ZONE = 14;
                final int MIN_ZONE = -11;
                showProgress(true);
                if(nTimezone < MIN_ZONE || nTimezone > MAX_ZONE) {
                    SystemUtility.showToast(EditInfoActivity.this, R.string.error_timezone);
                    return;
                }

                // Trigger update api
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ApiClient.UpdateTimezoneRet updateRet = ApiClient.getInst().updateTimezone(RESTAPIDemoSettings.getSessionToken(), RESTAPIDemoSettings.getObjId(), nTimezone);
                        Message msg;
                        if(updateRet == null) {
                            msg = Message.obtain();
                            msg.what = MSG_UPDATEZONE_FAIL;
                            mUIHandler.sendMessage(msg);
                        } else {
                            msg = Message.obtain();
                            RESTAPIDemoSettings.setTimezone(nTimezone);
                            msg.what = MSG_UPDATEZONE_SUCCESS;
                            msg.arg1 = nTimezone;
                            msg.obj = updateRet.mUpdateAt;
                            mUIHandler.sendMessage(msg);
                        }
                    }
                }).start();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();
    }

}
