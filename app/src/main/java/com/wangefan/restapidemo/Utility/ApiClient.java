package com.wangefan.restapidemo.Utility;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiClient {
    static private ApiClient mInst;
//    static private final RestAdapter mRestAdapter;
    static private UserApi mUserApi;

    private static class Config {
        private enum Env {
            PROD("watch-master-staging.herokuapp.com"),   //目前只有一個主機
            DEV("watch-master-staging.herokuapp.com"); //目前只有一個主機
            public final String host;
            Env(String host) {
                this.host = host;
            }
        }

        // 要去連的環境，這邊設定為正式環境, 將來會改這邊
        public final static Env env = Env.PROD;

        public static String getEndPoint() {
            return getEndPoint(Config.env);
        }

        public static String getEndPoint(Env env) {
            return String.format("https://%s/api/", env.host);
        }
    }

    public class LoginRet {
        @SerializedName("objectId")
        public String mObojectId;

        @SerializedName("_perishable_token")
        String mPerToken;

        @SerializedName("createdAt")
        String mCreateAt;

        @SerializedName("updatedAt")
        String mUpdateAt;

        @SerializedName("username")
        public String mUserName;

        @SerializedName("timezone")
        public int mTimezone;

        @SerializedName("timeZone")
        String mTimeZone;

        @SerializedName("sessionToken")
        public String mSnToken;
    }

    public class UpdateTimezoneRet {
        @SerializedName("updatedAt")
        public String mUpdateAt;
    }

    public interface UserApi {

        @Headers({
                "X-Parse-Application-Id: vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD",
                "X-Parse-REST-API-Key: "
        })
        @GET("login")
        Call<LoginRet> login(@Query("username") String userName, @Query("password") String pwd);

        @Headers({
                "X-Parse-Application-Id: vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD",
                "X-Parse-REST-API-Key: "
        })
        @PUT("users/{objid}")
        Call<UpdateTimezoneRet> updateTimezone(@Header("X-Parse-Session-Token") String sToken,
                                               @Path("objid") String objId,
                                               @Body JsonObject nTimezone);
    }

    private ApiClient() {
        //這邊的使用情境為全部 API 都對同一個 Server 操作
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl(Config.getEndPoint())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mUserApi = retrofit.create(UserApi.class);
    }

    static public ApiClient getInst() {
        if (mInst == null) {
            mInst = new ApiClient();
        }
        return mInst;
    }

    /**
     * 執行登入動作，should call in worker thread
     * @param userName
     * @param pwd
     * @return null if fail login
     * @throws IOException
     */
    public LoginRet login(String userName, String pwd) {
        try {
            return mUserApi.login(userName, pwd).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 執行update timezone動作，需在worker thread執行
     * @param seesionToken
     * @param objId, Object Id from login API
     * @param nTmiezone
     * @return null if fail.
     */
    public UpdateTimezoneRet updateTimezone(String seesionToken, String objId, int nTmiezone) {
        try {
            JsonObject req = new JsonObject();
            req.addProperty("timezone", nTmiezone);
            Response<UpdateTimezoneRet> res = mUserApi.updateTimezone(seesionToken, objId, req).execute();
            UpdateTimezoneRet body = res.body();
            return body;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}