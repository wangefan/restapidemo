package com.wangefan.restapidemo.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * 系統相關的Utility <br/>
 * 備註：檢查網路
 *
 */
public class SystemUtility {

    /**
     * 檢查目前有無網路的狀態
     *
     * @param context Context
     * @return 若有網路為true
     */
    public static boolean checkNetworkEnable(Context context) {
        ConnectivityManager mgrConnectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = mgrConnectivity.getActiveNetworkInfo();

        if (activeNetwork != null) {
            return true;
        } else {
            return false;
        }
    }

    public static void showToast(Context context, int resourceId) {
        Toast toast = Toast.makeText(context, resourceId, Toast.LENGTH_SHORT);
        toast.show();
    }
}
