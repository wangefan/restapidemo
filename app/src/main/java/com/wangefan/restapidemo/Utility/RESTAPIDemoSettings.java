package com.wangefan.restapidemo.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class RESTAPIDemoSettings {

    // 1. 常數 ==================================================================================
    public static final String RESTAPI_DEMO = "RESTAPI_DEMO";
    public static final String PREF_KEY_SESSION = "PREF_KEY_SESSION";
    public static final String PREF_KEY_OBJID = "PREF_KEY_OBJID";
    public static final String PREF_KEY_USERNAME = "PREF_KEY_USERNAME";
    public static final String PREF_KEY_TIMEZONE = "PREF_KEY_TIMEZONE";

    // 2. 其他變數宣告 ===============================================================================
    protected static SharedPreferences mSharedRestApiDemo = null;

    // 3. 建構子 ===============================================================================
    private RESTAPIDemoSettings() {
    }

    public static void init(Context context) {
        mSharedRestApiDemo = context.getSharedPreferences(RESTAPI_DEMO, Context.MODE_PRIVATE);
    }

    public static String getSessionToken() {
        String session = mSharedRestApiDemo.getString(PREF_KEY_SESSION, "");
        return session;
    }

    public static void setSessionToken(String session) {
        mSharedRestApiDemo.edit().putString(PREF_KEY_SESSION, session).commit();
    }

    public static String getObjId() {
        String objId = mSharedRestApiDemo.getString(PREF_KEY_OBJID, "");
        return objId;
    }

    public static void setObjId(String objId) {
        mSharedRestApiDemo.edit().putString(PREF_KEY_OBJID, objId).commit();
    }

    public static String getUserName() {
        return mSharedRestApiDemo.getString(PREF_KEY_USERNAME, "");
    }

    public static void setUserName(String userName) {
        mSharedRestApiDemo.edit().putString(PREF_KEY_USERNAME, userName).commit();
    }

    public static int getTimezone() {
        return mSharedRestApiDemo.getInt(PREF_KEY_TIMEZONE, 0);
    }

    public static void setTimezone(int nTimezone) {
        mSharedRestApiDemo.edit().putInt(PREF_KEY_TIMEZONE, nTimezone).commit();
    }
}
