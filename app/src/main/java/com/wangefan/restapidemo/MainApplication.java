package com.wangefan.restapidemo;

import android.app.Application;

import com.wangefan.restapidemo.Utility.RESTAPIDemoSettings;

/**
 * Created by wangefan on 19/5/2017.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // init SharePreference
        RESTAPIDemoSettings.init(this);
    }
}